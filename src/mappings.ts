import type { GeoJSONFeature } from 'ol/format/GeoJSON'

// Preferred over official QSelectOptions type
// to save ambiguity with Vue .value
export interface QSelectOptionsField {
  label: string
  id: number
}

export interface IdValueMap {
  [id: number]: string
}

export interface LabelValueMap {
  label: string
  value: number | string
}

export interface ElevationMinMax {
  min: number
  max: number
}

export type StackedSeries = {
  name: string
  data: Array<number>
}[]

export type MastIdValueMap = {
  [id: number]: Array<GeoJSONFeature>
}

export const mastMap: Array<QSelectOptionsField> = Array.from([
  {
    label: 'Fehlmast',
    id: 0,
  },
  {
    label: 'Sprengmast',
    id: 1,
  },
  {
    label: 'Halbmast',
    id: 2,
  },
  {
    label: 'Vollmast',
    id: 3,
  },
])

export const speciesMap: Array<QSelectOptionsField> = Array.from([
  {
    label: 'Abies alba',
    id: 1,
  },
  {
    label: 'Picea abies',
    id: 2,
  },
  {
    label: 'Fagus sylvatica',
    id: 3,
  },
  {
    label: 'Quercus petraea',
    id: 4,
  },
  {
    label: 'Quercus robur',
    id: 5,
  },
  {
    label: 'Acer campestre',
    id: 6,
  },
  {
    label: 'Acer opalus',
    id: 7,
  },
  {
    label: 'Acer platanoides',
    id: 8,
  },
  {
    label: 'Acer pseudoplatanus',
    id: 9,
  },
  {
    label: 'Alnus alnobetula',
    id: 10,
  },
  {
    label: 'Alnus cordata',
    id: 11,
  },
  {
    label: 'Alnus glutinosa',
    id: 12,
  },
  {
    label: 'Alnus incana',
    id: 13,
  },
  {
    label: 'Alnus viridis',
    id: 14,
  },
  {
    label: 'Betula humilis',
    id: 15,
  },
  {
    label: 'Betula pendula',
    id: 16,
  },
  {
    label: 'Betula pubescens',
    id: 17,
  },
  {
    label: 'Carpinus betulus',
    id: 18,
  },
  {
    label: 'Castanea sativa',
    id: 19,
  },
  {
    label: 'Corylus avellana',
    id: 20,
  },
  {
    label: 'Fraxinus excelsior',
    id: 21,
  },
  {
    label: 'Juglans regia',
    id: 22,
  },
  {
    label: 'Larix decidua',
    id: 23,
  },
  {
    label: 'Larix kaempferi',
    id: 24,
  },
  {
    label: 'Malus sylvestris',
    id: 25,
  },
  {
    label: 'Mespilus germanica',
    id: 26,
  },
  {
    label: 'Ostrya carpinifolia',
    id: 27,
  },
  {
    label: 'Picea omorika',
    id: 28,
  },
  {
    label: 'Pinus cembra',
    id: 29,
  },
  {
    label: 'Pinus mugo',
    id: 30,
  },
  {
    label: 'Pinus nigra',
    id: 31,
  },
  {
    label: 'Pinus strobus',
    id: 32,
  },
  {
    label: 'Pinus sylvestris',
    id: 33,
  },
  {
    label: 'Populus tremula',
    id: 34,
  },
  {
    label: 'Prunus avium',
    id: 35,
  },
  {
    label: 'Prunus domestica',
    id: 36,
  },
  {
    label: 'Pseudotsuga menziesii',
    id: 37,
  },
  {
    label: 'Pyrus pyraster',
    id: 38,
  },
  {
    label: 'Quercus pubescens',
    id: 39,
  },
  {
    label: 'Quercus rubra',
    id: 40,
  },
  {
    label: 'Robinia pseudoacacia',
    id: 41,
  },
  {
    label: 'Sorbus aria',
    id: 42,
  },
  {
    label: 'Sorbus aucuparia',
    id: 43,
  },
  {
    label: 'Sorbus domestica',
    id: 44,
  },
  {
    label: 'Sorbus latifolia',
    id: 45,
  },
  {
    label: 'Sorbus intermedia',
    id: 46,
  },
  {
    label: 'Sorbus torminalis',
    id: 47,
  },
  {
    label: 'Taxus baccata',
    id: 48,
  },
  {
    label: 'Thuja plicata',
    id: 49,
  },
  {
    label: 'Tilia cordata',
    id: 50,
  },
  {
    label: 'Tilia platyphyllos',
    id: 51,
  },
  {
    label: 'Tilia Sp',
    id: 52,
  },
  {
    label: 'Ulmus glabra',
    id: 53,
  },
  {
    label: 'Ulmus laevis',
    id: 54,
  },
  {
    label: 'Ulmus minor',
    id: 55,
  },
  {
    label: 'Abies Sp',
    id: 56,
  },
  {
    label: 'Picea Sp',
    id: 57,
  },
  {
    label: 'Fagus Sp',
    id: 58,
  },
  {
    label: 'Quercus Sp',
    id: 59,
  },
  {
    label: 'Acacieae Sp',
    id: 60,
  },
  {
    label: 'Acer Sp',
    id: 61,
  },
  {
    label: 'Alnus Sp',
    id: 62,
  },
  {
    label: 'Betula Sp',
    id: 63,
  },
  {
    label: 'Carpinus Sp',
    id: 64,
  },
  {
    label: 'Castanea Sp',
    id: 65,
  },
  {
    label: 'Corylus Sp',
    id: 66,
  },
  {
    label: 'Fraxinus Sp',
    id: 67,
  },
  {
    label: 'Juglans Sp',
    id: 68,
  },
  {
    label: 'Larix Sp',
    id: 69,
  },
  {
    label: 'Malus Sp',
    id: 70,
  },
  {
    label: 'Mespilus Sp',
    id: 71,
  },
  {
    label: 'Ostrya Sp',
    id: 72,
  },
  {
    label: 'Pinus Sp',
    id: 73,
  },
  {
    label: 'Populus Sp',
    id: 74,
  },
  {
    label: 'Prunus Sp',
    id: 75,
  },
  {
    label: 'Pseudotsuga Sp',
    id: 76,
  },
  {
    label: 'Pyrus Sp',
    id: 77,
  },
  {
    label: 'Robinia Sp',
    id: 78,
  },
  {
    label: 'Sorbus Sp',
    id: 79,
  },
  {
    label: 'Taxus Sp',
    id: 80,
  },
  {
    label: 'Thuja Sp',
    id: 81,
  },
  {
    label: 'Ulmus Sp',
    id: 82,
  },
])
