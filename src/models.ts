import { QSelectOptionsField } from '@/mappings'

export interface NewSite {
  coord: Array<number>
  genus: string | null
  species: QSelectOptionsField | null
  mastType: QSelectOptionsField | null
  mastYear: string
  dateObservation: string
  observer: string | null
  systematic: boolean
  imageUrl: string | null
  inStand: boolean
  comments: string | null
}

export interface SiteForAPI {
  geohash: string | null
  speciesId: number | null
  mastTypeId: number | null
  mastYear: string
  dateObservation: Date
  observer: string | null
  comments: string | null
  systematic: boolean
  imageUrl: string | null
  inStand: boolean
  userId: string | null
}

// Base class to extend SiteDB and SiteData
export interface SiteBase {
  id: number
  coord: Array<number> | string
  speciesId: number
  mastTypeId: number
  mastYear: number
  dateObservation: string
  systematic: boolean
  imageUrl: string | null
  inStand: boolean
  dataSource: string
  elevation: number | null
  slope: number | null
  eastness: number | null
  northness: number | null
  canton: string | null
  municipality: string | null
  vegetationBelt: string | null
  biogeographicRegion: string | null
}

export interface SiteData extends SiteBase {
  coord: string
}

export interface SiteDB extends SiteBase {
  coord: Array<number>
}

export interface UserAccount {
  displayName: string
  email: string
  familyName: string
  givenName: string
  locale: string
  name: string
  nickname: string
  picture: string
  subscribed: boolean
}
