import { RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'map', component: () => import('@/pages/MapPage.vue') },
      { path: '/profile', name: 'profile', component: () => import('@/pages/ProfilePage.vue') },
      { path: '/new', name: 'new', component: () => import('@/pages/NewSitePage.vue') },
      { path: '/data', name: 'data', component: () => import('@/pages/DataPage.vue') },
      { path: '/logout', name: 'logout', component: () => import('@/pages/LogoutPage.vue') },
    ],
  },
  {
    path: '/:catchAll(.*)*',
    name: '404',
    component: () => import('@/pages/ErrorNotFound.vue'),
  },
]

export default routes
