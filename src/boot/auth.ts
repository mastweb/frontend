import { Notify } from 'quasar'
import { boot } from 'quasar/wrappers'
import { Network } from '@capacitor/network'
import { createAuth0 } from '@auth0/auth0-vue'
import { useUserStore } from 'src/stores/user'

const domain = import.meta.env.APP_AUTH0_DOMAIN
const clientId = import.meta.env.APP_AUTH0_CLIENT_ID
const audience = import.meta.env.APP_AUTH0_API_AUDIENCE

export const auth0 = createAuth0({
  domain,
  clientId: clientId,
  authorizationParams: {
    redirect_uri: window.location.origin,
    audience,
  },
})

export default boot(({ app, router, store }) => {
  // Set auth0 instance on app
  app.use(auth0)

  const userStore = useUserStore(store)

  // Add Auth guard to selected routes
  router.beforeEach(async (to) => {
    if (to.path === '/profile' || to.path === '/new') {
      if (userStore.authAccount?.email_verified === false) {
        Notify.create({
          color: 'accent',
          position: 'bottom',
          message: 'Please verify your email first.',
          caption: 'Then login again.',
          actions: [
            {
              icon: 'login',
              'aria-label': 'Logout',
              color: 'secondary',
              handler: async () => {
                // Cycle login to test if email verified
                await userStore.logout()
                await userStore.login()
              },
            },
          ],
        })

        // Block access
        return false
      } else {
        const networkStatus = await Network.getStatus()
        if (networkStatus.connected) {
          // Online: check if logged in
          const loggedIn = await userStore.loadUserIfAvailable()
          if (!loggedIn) {
            userStore.login()
          }
        } else {
          if (userStore.loggedIn()) {
            // Offline: basic login check,
            // then allow offline access
            return true
          }
        }
      }
    }
    return true
  })
})
