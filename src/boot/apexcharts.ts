import { boot } from 'quasar/wrappers'
import VueApexCharts from 'vue3-apexcharts'
import en from 'apexcharts/dist/locales/en.json'
import de from 'apexcharts/dist/locales/de.json'
import fr from 'apexcharts/dist/locales/fr.json'
import it from 'apexcharts/dist/locales/it.json'

export default boot(({ app }) => {
  app.use(VueApexCharts)

  // Set locales
  window.Apex.chart = {
    locales: [en, de, fr, it],
  }
})
