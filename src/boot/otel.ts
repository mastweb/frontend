import { WebTracerProvider } from '@opentelemetry/sdk-trace-web'
import { getWebAutoInstrumentations } from '@opentelemetry/auto-instrumentations-web'
import { OTLPTraceExporter } from '@opentelemetry/exporter-trace-otlp-http'
import { BatchSpanProcessor } from '@opentelemetry/sdk-trace-base'
import { registerInstrumentations } from '@opentelemetry/instrumentation'
import { ZoneContextManager } from '@opentelemetry/context-zone'
import { Resource } from '@opentelemetry/resources'
import { SemanticResourceAttributes } from '@opentelemetry/semantic-conventions'

function initOtel(otelUrl: string, backendUrl: string) {
  const exporter = new OTLPTraceExporter({
    url: otelUrl,
  })
  const provider = new WebTracerProvider({
    resource: new Resource({
      [SemanticResourceAttributes.SERVICE_NAME]: 'mastweb',
    }),
  })
  provider.addSpanProcessor(new BatchSpanProcessor(exporter))
  provider.register({
    contextManager: new ZoneContextManager(),
  })

  registerInstrumentations({
    instrumentations: [
      getWebAutoInstrumentations({
        // load custom configuration for xml-http-request instrumentation
        '@opentelemetry/instrumentation-xml-http-request': {
          propagateTraceHeaderCorsUrls: [backendUrl],
        },
        // load custom configuration for fetch instrumentation
        '@opentelemetry/instrumentation-fetch': {
          propagateTraceHeaderCorsUrls: [backendUrl],
        },
      }),
    ],
  })
}

if (process.env.PROD) {
  const otelUrl = import.meta.env.APP_OTEL_TRACE_ENDPOINT
  const backendUrl = import.meta.env.APP_API_URL
  initOtel(otelUrl, backendUrl)
}
