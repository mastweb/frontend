import Dexie from 'dexie'
import type { Table } from 'dexie'
import type { SiteData, SiteDB, SiteForAPI } from '@/models'
import { GeoJSONFeature } from 'ol/format/GeoJSON'

class DexieSites extends Dexie {
  sites!: Table<SiteData>
  offlineSites!: Table<SiteForAPI>

  constructor() {
    super('siteDB')
    this.version(1).stores({
      sites: 'id',
      offlineSites: '++id',
    })
  }
}

export const db = new DexieSites()

export async function getSitesFromDb() {
  const dbSites = await db.table('sites').toArray()

  if (dbSites.length === 0) {
    // No sites
    return []
  } else {
    // Transform to GeoJSON
    return dbSites.map((site: SiteDB) => {
      const { id, coord, ...properties } = site
      return {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: coord,
        },
        id: id,
        properties: properties,
      }
    })
  }
}

export function clearSitesInDb() {
  // Clear IndexDB
  db.table('sites').clear()
}

export function addSitesToDb(sites: Array<GeoJSONFeature>) {
  // Add GeoJSON site array to IndexDB
  db.table('sites').bulkAdd(
    JSON.parse(
      JSON.stringify(
        sites.map((x: GeoJSONFeature) => ({
          id: x.id,
          coord: x.geometry.coordinates,
          ...x.properties,
        }))
      )
    )
  )
}

export async function getOfflineSitesFromDb() {
  const dbSites = await db.table('offlineSites').toArray()
  // Remove primary key id, as generated on backend
  return dbSites.map((site) => {
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
    const { id, ...vars } = site
    return vars
  })
}

export function clearOfflineSitesInDb() {
  // Clear IndexDB
  db.table('offlineSites').clear()
}

export async function addOfflineSiteToDb(site: SiteForAPI) {
  // Add SiteForAPI site to IndexDB
  try {
    await db.table('offlineSites').add(site)
    return true
  } catch (error) {
    console.error(error)
    console.error('Error adding site to IndexedDB')
    return false
  }
}
