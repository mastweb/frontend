import { boot } from 'quasar/wrappers'
import axios from 'axios'
import type { AxiosResponse, AxiosRequestConfig } from 'axios'
import { camelizeKeys, decamelizeKeys } from 'humps'

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
const api = axios.create({
  baseURL: import.meta.env.DEV ? 'http://localhost:9111' : import.meta.env.APP_API_URL,
})

// Axios middleware to convert all api responses to camelCase
api.interceptors.response.use((response: AxiosResponse) => {
  if (response.data && response.headers['content-type'] === 'application/json') {
    response.data = camelizeKeys(response.data)
  }

  return response
})

// Axios middleware to convert all api requests to snake_case
api.interceptors.request.use((config: AxiosRequestConfig) => {
  if (config.headers['Content-Type'] === 'multipart/form-data') return config

  if (config.params) {
    config.params = decamelizeKeys(config.params)
  }

  if (config.data) {
    config.data = decamelizeKeys(config.data)
  }

  return config
})

export { api }

export default boot(({}) => {
  return
})
