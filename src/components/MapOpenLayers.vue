<template>
  <div ref="mapParentDiv" :class="isMinimap ? 'minimap' : 'absolute-full'"></div>
</template>

<script setup lang="ts">
  import { watch, onMounted, toRaw } from 'vue'
  import type { PropType } from 'vue'

  import 'ol/ol.css'
  import 'ol-ext/dist/ol-ext.css'
  import { Map, View } from 'ol'
  import { defaults as defaultControls } from 'ol/control'
  import { Draw } from 'ol/interaction'
  import { transform as transformCoord } from 'ol/proj'
  import LayerSwitcherImage from 'ol-ext/control/LayerSwitcherImage'
  import { createEmpty, extend, getWidth } from 'ol/extent'
  import { Vector as VectorLayer } from 'ol/layer'
  import BaseLayer from 'ol/layer/Base'
  import TileLayer from 'ol/layer/WebGLTile.js'
  import { OSM, Cluster, Vector as VectorSource } from 'ol/source'
  import AnimatedCluster from 'ol-ext/layer/AnimatedCluster'
  import SelectCluster from 'ol-ext/interaction/SelectCluster'
  import LayerGroup from 'ol/layer/Group'
  import Feature from 'ol/Feature'
  import GeoJSON from 'ol/format/GeoJSON'
  import type { GeoJSONFeature } from 'ol/format/GeoJSON'
  import type { Type as GeomType } from 'ol/geom/Geometry'
  import { Circle as CircleStyle, Stroke, Fill, Text, Style } from 'ol/style'
  import { asArray as asColorArray } from 'ol/color'
  import Legend from 'ol-ext/legend/Legend'
  import LegendControl from 'ol-ext/control/Legend'

  import { geojson as FGBGeoJson } from 'flatgeobuf'

  interface IdValueMap {
    [id: number]: string
  }

  const props = defineProps({
    geoJsonArray: {
      type: Array as PropType<Array<GeoJSON>>,
      default: () => [],
    },
    clusterData: {
      type: Boolean as PropType<boolean>,
      default: true,
    },
    clusterMarkerColorParam: {
      type: String as PropType<string>,
      default: undefined,
    },
    clusterMarkerColorMap: {
      type: Object as PropType<IdValueMap>,
      default: undefined,
    },
    clusterZoomOnClick: {
      type: Boolean as PropType<boolean>,
      default: true,
    },
    isMinimap: {
      type: Boolean as PropType<boolean>,
      default: false,
    },
    drawEnabled: {
      type: Boolean as PropType<boolean>,
      default: false,
    },
    baseMap: {
      type: String as PropType<string>,
      default: 'OSM',
    },
    mapCenter: {
      type: Array as PropType<Array<number>>,
      default: () => [],
    },
    primaryColor: {
      type: String as PropType<string>,
      default: '#007b83',
    },
    secondaryColor: {
      type: String as PropType<string>,
      default: '#fefefe',
    },
    accentColor: {
      type: String as PropType<string>,
      default: '#e7e5ac',
    },
    legendIdNameMap: {
      type: Object as PropType<IdValueMap>,
      default: undefined,
    },
    flatgeobufUrl: {
      type: String as PropType<string>,
      default: undefined,
    },
    flatgeobufFieldName: {
      type: String as PropType<string>,
      default: 'value',
    },
    flatgeobufColorRamp: {
      type: Array as PropType<Array<string>>,
      default: undefined,
    },
  })
  const emit = defineEmits<{
    (_e: 'drawShape', _value: Array<number> | null): void
    (_e: 'clickModalPopup', _featureProps: Record<string, unknown>): void
  }>()

  const map = $ref(new Map({ controls: defaultControls({ zoom: false }) }))
  defineExpose({ map })
  const mapParentDiv = $ref<HTMLElement>()

  const drawEnabled = $toRef(props, 'drawEnabled')
  let draw: Draw = $ref<Draw>()
  let drawLayer: BaseLayer | null = $ref<VectorLayer<VectorSource> | null>(null)
  let drawPolygonCoords: Array<number> | null = $ref<Array<number> | null>(null)

  const geoJsonArray = $toRef(props, 'geoJsonArray')
  let mapGeoJsonLayers: Array<BaseLayer> = $ref<Array<VectorLayer<Cluster | VectorSource>>>()
  // let mapTileLayer: TileLayer = $ref<TileLayer>()
  let mapFgbLayer: VectorLayer<VectorSource> = $ref<VectorLayer<VectorSource>>()
  const baseMap = $toRef(props, 'baseMap')
  const mapCenter = $toRef(props, 'mapCenter')

  watch(
    () => mapCenter,
    (newCenter: Array<number>) => {
      if (newCenter) {
        const coord4326 = transformCoord(newCenter, 'EPSG:4326', 'EPSG:3857')
        map.getView().animate({ center: coord4326, zoom: 16 })
      }
    }
  )

  watch(
    () => drawEnabled,
    (drawEnabled: boolean) => {
      if (drawEnabled) {
        addDrawToMap()
      } else {
        removeDrawFromMap()
      }
    }
  )

  function addDrawToMap(geomType: GeomType = 'Polygon') {
    const drawSource = new VectorSource({ wrapX: false })
    drawLayer = new VectorLayer({
      source: drawSource,
      zIndex: 20,
    })
    map.addLayer(drawLayer)

    draw = new Draw({
      source: drawSource,
      type: geomType,
    })
    map.addInteraction(draw)

    draw.on('drawstart', function () {
      drawSource.clear()
    })

    drawSource.on('addfeature', function () {
      const geom = drawLayer?.getSource()?.getFeatures()[0]?.getGeometry()

      if (geom) {
        drawPolygonCoords = geom.clone().transform('EPSG:3857', 'EPSG:4326').getCoordinates()
        if (geom.getType() === 'Polygon') {
          emit('drawShape', drawPolygonCoords)
        }

        // Untoggle draw interaction
        map.removeInteraction(draw)
      }
    })
  }

  function removeDrawFromMap() {
    map.removeLayer(drawLayer)
    map.removeInteraction(draw)
    drawLayer = null
    drawPolygonCoords = null
  }

  // Cluster Style
  interface ClusterStyleCache {
    [numPoints: number]: Style
  }
  const styleCache: ClusterStyleCache = {}
  /**
   * Static cluster styles, without click interaction.
   * Either cluster style, or single point style.
   * @param {Feature} feature A feature from a cluster.
   * @param {number} _resolution A resolution.
   * @return {Style} An style associated with the cluster feature.
   */
  function clusterStyle(feature: Feature, _resolution: number): Style {
    const size = feature.get('features').length
    let style: Style = styleCache[size]

    if (style) {
      return style
    }

    if (size > 1) {
      // Zoomable cluster blob
      if (!style) {
        const radius = Math.max(14, Math.min(size * 0.75, 20))
        style = styleCache[size] = new Style({
          image: new CircleStyle({
            radius: radius,
            stroke: new Stroke({
              color: props.secondaryColor,
              width: 1,
            }),
            fill: new Fill({
              // Make translucent
              color: asColorArray(props.primaryColor).slice(0, -1).concat([0.7]),
            }),
          }),
          text: new Text({
            text: size.toString(),
            font: 'bold 10px Roboto',
            fill: new Fill({
              color: '#fff',
            }),
          }),
        })
      }
      return style
    } else {
      // Single feature zoomed in
      const originalFeature = feature.get('features')[0]
      const featureColor = getFeatureColor(originalFeature)
      return clusterSingleFeatureStyle(originalFeature, featureColor)
    }
  }

  /**
   * Get the feature color, determined by prop.
   * @param {Feature} clusterMember A feature from a cluster.
   * @return {Array<number>} A color array.
   */
  function getFeatureColor(clusterMember: Feature): Array<number> {
    if (props.clusterMarkerColorParam && props.clusterMarkerColorMap) {
      const featureColor =
        props.clusterMarkerColorMap[clusterMember.get(props.clusterMarkerColorParam)]
      return asColorArray(featureColor)
    }

    return asColorArray(props.primaryColor)
  }

  const primaryColorArray = asColorArray(props.primaryColor)
  /**
   * Single feature style, users for clusters with 1 feature and cluster circles.
   * @param {Feature} clusterMember A feature from a cluster.
   * @param {Array<number>} fillColor The OpenLayers output from asColorArray for circle fill.
   * @return {Style} An icon style for the cluster member's location.
   */
  function clusterSingleFeatureStyle(
    clusterMember: Feature,
    fillColor: Array<number> | null = null
  ): Style {
    if (typeof fillColor === 'number') {
      // Call for expandedFeatures pass a resolution instead
      const isExpandedFeature = clusterMember.get('selectclusterfeature')

      if (isExpandedFeature) {
        const feature = clusterMember.getProperties().features[0]
        fillColor = getFeatureColor(feature)
      } else {
        fillColor = primaryColorArray
      }
    }

    return new Style({
      // geometry: clusterMember.getGeometry(),
      image: new CircleStyle({
        radius: 10,
        fill: new Fill({
          color: fillColor,
        }),
        stroke: new Stroke({ color: props.secondaryColor, width: 1 }),
      }),
    })
  }

  // Select interaction to spread cluster out and select features
  const selectCluster = new SelectCluster({
    selectCluster: false, // disable cluster selection
    circleMaxObjects: 20,
    pointRadius: 20,
    spiral: false,
    animate: true,
    autoClose: true,
    // Feature style when springs apart
    featureStyle: clusterSingleFeatureStyle,
    // Style when clicked
    style: function (feature: Feature, _resolution: number) {
      const clusterMembers = feature.get('features')

      // Handle exceptions on draw layer
      if (!clusterMembers) {
        return
      }

      if (clusterMembers.length === 1) {
        // Maintain the style when individual clicked
        const expandedFeature = clusterMembers[0]
        const featureColor = getFeatureColor(expandedFeature)
        return clusterSingleFeatureStyle(expandedFeature, featureColor)
      }
    },
  })
  map.addInteraction(selectCluster)

  selectCluster.getFeatures().on('add', function (event) {
    const featuresClicked = event?.element?.get('features')

    if (!featuresClicked) {
      // Prevent other map layers disappearing
      selectCluster.clear()
      return
    }

    if (featuresClicked.length > 1 && props.clusterZoomOnClick) {
      // Calculate the extent of the cluster members.
      const extent = createEmpty()
      featuresClicked.forEach((feature: Feature) =>
        extend(extent, feature?.getGeometry()?.getExtent() ?? [])
      )
      const view = map.getView()
      const resolution = view.getResolution()
      if (view.getZoom() === view.getMaxZoom() || getWidth(extent) < resolution) {
        // Do nothing, allow spring out hidden members around radius
        return
      } else {
        view.fit(extent, { duration: 500, padding: [50, 50, 50, 50] })
        setTimeout(function () {
          // Manual override / clear expanded cluster, delay
          selectCluster.clear()
        }, 100)
        return
      }
    }

    if (featuresClicked.length !== 1) {
      // Clicked location with multiple features, but props say ignore
      return
    }

    // Display feature value
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
    const { geometry: geom, ...featureProps } = featuresClicked[0].getProperties()
    const coords = geom.getCoordinates()
    featureProps.coord = `${coords[1]}, ${coords[0]}`
    emit('clickModalPopup', featureProps)
  })

  map.on('moveend', (_event) => {
    selectCluster.clear()
  })

  function getClusterFromGeoJSONFeatures(
    geoJsonArray: Array<GeoJSONFeature>
  ): Array<AnimatedCluster> {
    let featureArray: Array<Feature> = []

    geoJsonArray.forEach((geoJson: GeoJSONFeature) => {
      featureArray.push(
        new GeoJSON().readFeature(geoJson, {
          dataProjection: 'EPSG:4326',
          featureProjection: 'EPSG:3857',
        })
      )
    })

    const clusterSource = new Cluster({
      distance: 35,
      // minDistance: 10,
      source: new VectorSource({
        features: featureArray,
      }),
    })

    const animatedCluster = new AnimatedCluster({
      name: 'Mast Sites',
      source: clusterSource,
      animationDuration: 700,
      style: clusterStyle,
    })

    return [animatedCluster]
  }

  function addGeoJsonLayers(geoJsons: Array<GeoJSONFeature>) {
    let legendLayer: VectorLayer<Cluster | VectorSource>
    if (geoJsons.length !== 0) {
      if (props.clusterData) {
        // Returns multiple layers: hulls, cluster groups, cluster points
        mapGeoJsonLayers = getClusterFromGeoJSONFeatures(toRaw(geoJsons))
        legendLayer = mapGeoJsonLayers[mapGeoJsonLayers.length - 1]
      } else {
        // TODO implement non-clustered layer
        mapGeoJsonLayers = []
        legendLayer = []
      }

      mapGeoJsonLayers.forEach((layer: VectorLayer<Cluster | VectorSource>) => {
        layer.setZIndex(50)
        map.addLayer(layer)
      })

      // Add legend
      if (props.legendIdNameMap && props.clusterMarkerColorMap) {
        const legend = new Legend({
          title: 'Legend',
          margin: 5,
          maxWidth: 300,
          layer: legendLayer,
        })
        const legendCtl = new LegendControl({
          legend: legend,
          // collapsed: false,
        })
        map.addControl(legendCtl)
        // Legend associated with cluster layer
        for (const [id, name] of Object.entries(props.legendIdNameMap)) {
          legend.addItem({
            title: name,
            typeGeom: 'Point',
            style: new Style({
              image: new CircleStyle({
                radius: 10,
                fill: new Fill({
                  color: asColorArray(props.clusterMarkerColorMap[id]),
                }),
                stroke: new Stroke({ color: props.secondaryColor, width: 1 }),
              }),
            }),
            className: '',
            textStyle: new Text({
              font: '14px Roboto',
              fill: new Fill({
                color: '#fff',
              }),
            }),
          })
        }
      }
    }
  }

  // function addCogLayer(cogUrl: string) {
  //   map.removeLayer(mapTileLayer)

  //   mapTileLayer = new TileLayer({
  //     source: new GeoTIFF({
  //       sources: [
  //         {
  //           url: cogUrl,
  //         },
  //       ],
  //       normalize: false,
  //     }),
  //   })

  //   mapTileLayer.setZIndex(10)
  //   map.addLayer(mapTileLayer)
  // }

  // function removeCogLayer() {
  //   map.removeLayer(mapTileLayer)
  // }

  const colorRamp = props.flatgeobufColorRamp
    ? props.flatgeobufColorRamp
    : ['blue', 'green', 'yellow', 'orange', 'red', 'purple']
  const flatgeobufStyle = function (feature: Feature) {
    const value = feature.get(props.flatgeobufFieldName)
    const colorIndex = Math.floor(value) % colorRamp.length
    const color = colorRamp[colorIndex]

    return new Style({
      fill: new Fill({
        color: asColorArray(color).slice(0, -1).concat([0.8]),
      }),
    })
  }

  function addFgbLayer(fgbUrl: string) {
    map.removeLayer(mapFgbLayer)

    const fgbSource = new VectorSource({
      loader: async function () {
        const response = await fetch(fgbUrl)
        for await (let feature of FGBGeoJson.deserialize(response.body)) {
          this.addFeature(
            new GeoJSON().readFeature(feature, {
              dataProjection: 'EPSG:4326',
              featureProjection: 'EPSG:3857',
            })
          )
        }
      },
    })

    mapFgbLayer = new VectorLayer({
      source: fgbSource,
      style: flatgeobufStyle,
      zIndex: 10,
    })

    map.addLayer(mapFgbLayer)
  }

  function removeFgbLayer() {
    map.removeLayer(mapFgbLayer)
  }

  const baseLayers = new LayerGroup({
    layers: [
      new TileLayer({
        title: 'OSM',
        baseLayer: true,
        source: new OSM({
          url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png',
          attributions: [
            '© <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>',
          ],
        }),
        visible: baseMap === 'OSM' ? true : false,
      }),
      new TileLayer({
        title: 'OpenTopo',
        baseLayer: true,
        source: new OSM({
          url: 'https://tile.opentopomap.org/{z}/{x}/{y}.png',
          attributions: [],
        }),
        visible: baseMap === 'OpenTopo' ? true : false,
      }),
      new TileLayer({
        title: 'ArcGIS',
        baseLayer: true,
        source: new OSM({
          url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
          attributions: ['© <a href="https://www.esri.com" target="_blank">ESRI</a>'],
        }),
        visible: baseMap === 'ArcGIS' ? true : false,
      }),
      // // TODO requires reprojecting
      // new TileLayer({
      //   title: 'ArcGIS',
      //   baseLayer: true,
      //   source: new OSM({
      //     url: 'https://tile.osm.ch/2056/{z}/{x}/{y}.png',
      //     attributions: ['© <a href="https://www.esri.com" target="_blank">ESRI</a>'],
      //     projection: 'EPSG:4326',
      //   }),
      //   visible: baseMap === 'CH' ? true : false,
      // }),
    ],
  })
  baseLayers.setZIndex(0)

  onMounted(() => {
    map.setTarget(mapParentDiv)
    map.setView(
      new View({
        center: transformCoord(
          mapCenter.length !== 0 ? mapCenter : [8.23, 46.82],
          'EPSG:4326',
          'EPSG:3857'
        ),
        zoom: 8,
        projection: 'EPSG:3857',
        constrainResolution: true,
      })
    )

    map.addLayer(baseLayers)
    map.addControl(new LayerSwitcherImage({ layerGroup: baseLayers }))

    addGeoJsonLayers(geoJsonArray)

    watch(
      () => geoJsonArray,
      (geoJsonArray: Array<GeoJSON>) => {
        // Remove layers prior to re-adding on change
        if (mapGeoJsonLayers) {
          mapGeoJsonLayers.forEach((layer: VectorLayer<Cluster | VectorSource>) => {
            map.removeLayer(layer)
          })
        }
        addGeoJsonLayers(geoJsonArray)
      }
    )

    watch(
      () => props.flatgeobufUrl,
      (flatgeobufUrl: string | undefined) => {
        if (flatgeobufUrl) {
          addFgbLayer(flatgeobufUrl)
        } else {
          removeFgbLayer()
        }
      }
    )
  })
</script>
