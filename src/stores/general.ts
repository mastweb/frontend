import { Notify, Loading } from 'quasar'
import type { QLoadingShowOptions, QNotifyUpdateOptions } from 'quasar'
import { defineStore } from 'pinia'
import type { GeoJSONFeature } from 'ol/format/GeoJSON'
import type { AxiosProgressEvent, AxiosResponse } from 'axios'

import { api } from '@/boot/axios'
import { i18n } from '@/boot/i18n'
import { getSitesFromDb, clearSitesInDb, addSitesToDb } from '@/boot/db'

type GeneralState = {
  useSidebar: boolean
  sidebarOpen: boolean
  isOnline: boolean
  sites: Array<GeoJSONFeature>
}

function showLoadingSpinner(
  isFirstLoad: boolean
): (
  _?:
    | Omit<QNotifyUpdateOptions, 'spinner' | 'spinnerSize'>
    | Omit<QLoadingShowOptions, 'spinnerSize'>
) => void {
  const { t } = i18n.global

  if (isFirstLoad) {
    return Loading.show({
      message: '0%',
    })
  }

  return Notify.create({
    group: false,
    color: 'primary',
    position: 'center',
    message: t('map.loading') + '...',
    caption: '0%',
    spinner: true,
    timeout: 0,
  })
}

function hideLoadingSpinner(
  isFirstLoad: boolean,
  hideSpinner: (
    _?:
      | Omit<QNotifyUpdateOptions, 'spinner' | 'spinnerSize'>
      | Omit<QLoadingShowOptions, 'spinnerSize'>
  ) => void
) {
  if (isFirstLoad) {
    Loading.hide()
  } else {
    hideSpinner()
  }
}

export const useGeneralStore = defineStore<string, GeneralState>('general', {
  state: () => ({
    useSidebar: false,
    sidebarOpen: false,
    isOnline: true,
    sites: [],
  }),
  actions: {
    resetState() {
      this.$patch({
        useSidebar: false,
        sidebarOpen: false,
        isOnline: true,
        sites: [],
      })
    },
    setOnline() {
      this.$patch({
        isOnline: true,
      })
    },
    setOffline() {
      this.$patch({
        isOnline: false,
      })
    },
    async loadSites(isFirstLoad: boolean) {
      // Load sites

      const sitesLoading = showLoadingSpinner(isFirstLoad)

      // Recover from IndexedDB, if present
      if (isFirstLoad) {
        const dbSites: Array<GeoJSONFeature> = await getSitesFromDb()

        if (dbSites.length !== 0) {
          this.$patch({
            sites: dbSites,
          })
          hideLoadingSpinner(isFirstLoad, sitesLoading)
          return
        }
      }

      // Else call API
      clearSitesInDb()

      api
        .get('/data', {
          onDownloadProgress: (progressEvent: AxiosProgressEvent) => {
            const percentComplete = Math.round(
              (progressEvent.loaded * 100) / (progressEvent.total ? progressEvent.total : 100)
            )
            sitesLoading({
              ...(!isFirstLoad && { caption: `${percentComplete}%` }),
              ...(isFirstLoad && { message: `${percentComplete}%` }),
            })
          },
        })
        .then((response: AxiosResponse) => {
          // Add to store
          this.$patch({
            sites: response.data,
          })
          // Attempt to add to IndexDB
          addSitesToDb(response.data)
        })
        .catch(() => {
          const { t } = i18n.global

          Notify.create({
            color: 'negative',
            position: 'bottom',
            message: t('map.failed'),
            icon: 'report_problem',
          })
        })
        .finally(() => {
          hideLoadingSpinner(isFirstLoad, sitesLoading)
        })
    },
  },
})
