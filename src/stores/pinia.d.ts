import 'pinia'

declare module 'pinia' {
  export interface PiniaCustomProperties {
    resetUser: () => void
    resetState: () => void
    setOnline: () => void
    setOffline: () => void
    showLoadingSpinner: (_: boolean) => void
    hideLoadingSpinner: (_: boolean) => void
    loadSites: (_: boolean) => void
    login: () => Promise<void>
    logout: () => Promise<void>
    isAuthenticated: () => Promise<boolean>
    loggedIn: () => boolean
    loadUserIfAvailable: () => void
  }
}
