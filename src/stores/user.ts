import { date } from 'quasar'
import { ref, unref, toRaw, watchEffect } from 'vue'
import { defineStore } from 'pinia'
import { useLocalStorage, useSessionStorage } from '@vueuse/core'
import type { RemovableRef } from '@vueuse/core'
import { User as Auth0User } from '@auth0/auth0-spa-js'

import { auth0 } from '@/boot/auth'
import { UserAccount } from '@/models'

export function watchEffectOnceAsync<T>(watcher: () => T) {
  return new Promise<void>((resolve) => {
    watchEffectOnce(watcher, resolve)
  })
}

export function watchEffectOnce<T>(watcher: () => T, fn: () => void) {
  const stopWatch = watchEffect(() => {
    if (watcher()) {
      fn()
      stopWatch()
    }
  })
}

type UserState = {
  authAccount: RemovableRef<Auth0User>
  apiAccount: RemovableRef<UserAccount>
  accessToken: RemovableRef<string>
  tokenExpiry: RemovableRef<Date>
  currentLocale: RemovableRef<string>
  currentLocation: RemovableRef<Array<number>>
  baseMap: RemovableRef<string>
}
export const useUserStore = defineStore<string, UserState>('user', {
  state: () => ({
    authAccount: useLocalStorage('authAccount', {}),
    apiAccount: useLocalStorage(
      'apiAccount',
      ref<UserAccount>({
        displayName: '',
        email: '',
        familyName: '',
        givenName: '',
        locale: '',
        name: '',
        nickname: '',
        picture: '',
        subscribed: false,
      })
    ),
    accessToken: useSessionStorage('accessToken', ''),
    tokenExpiry: useSessionStorage('tokenExpiry', new Date()),
    currentLocale: useLocalStorage('currentLocale', ''),
    currentLocation: useSessionStorage('currentLocation', []),
    baseMap: useLocalStorage('baseMap', 'OSM'),
  }),
  getters: {
    // isAuthenticated: (state) => state.authAccount !== null,
    // TODO get names from database
    // fullName: (state) => `${state.authAccount?.given_name} ${state.authAccount?.family_name}`,
  },
  actions: {
    resetUser() {
      this.$patch({
        authAccount: {},
        apiAccount: {},
        accessToken: '',
        tokenExpiry: new Date(),
        currentLocale: '',
        currentLocation: [],
        baseMap: 'OSM',
      })
    },
    async login() {
      if (!this.loggedIn()) {
        await auth0.loginWithRedirect()
      }
    },
    async logout() {
      if (this.loggedIn()) {
        // Manually reset user for reactivity
        this.resetUser()
        this.authAccount = {}
        // Logout redirect
        await auth0.logout({
          logoutParams: {
            returnTo: window.location.origin,
          },
        })
      }
    },
    async isAuthenticated() {
      const fn = async () => {
        if (unref(auth0.isAuthenticated)) {
          return true
        }
      }

      if (!unref(auth0.isLoading)) {
        return fn()
      }

      await watchEffectOnceAsync(() => !unref(auth0.isLoading))

      return fn()
    },
    async loadUserIfAvailable() {
      // Load in user if missing
      if (Object.keys(toRaw(this.authAccount)).length === 0) {
        const isAuthenticated = await this.isAuthenticated()
        if (!isAuthenticated) {
          return
        }
        this.$patch({
          authAccount: unref(auth0.user),
        })
      }
      // Get new access token silently, if possible
      try {
        if (!this.loggedIn()) {
          this.$patch({
            accessToken: await auth0.getAccessTokenSilently(),
            tokenExpiry: date.addToDate(new Date(), { days: 1 }),
          })
        }
        return this.loggedIn()
      } catch {
        return false
      }
    },
    loggedIn() {
      if (!this.authAccount) {
        // In case where authAccount is empty can't use Object.keys
        return false
      }
      if (
        Object.keys(toRaw(this.authAccount)).length !== 0 &&
        this.accessToken &&
        this.tokenExpiry > new Date()
      ) {
        return true
      }
      return false
    },
  },
})
