# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## v1.2.3 (2023-08-09)

### Fix

- use q-input for newpage dateobs, fix popup
- allow offline access to the /new site page
- improve check for authAccount.picture in layout

## v1.2.2 (2023-07-31)

### Fix

- newsite page remove clearable species select
- make newsite upload button central on mobile
- add link to privacy policy on profile page

### Refactor

- link to mastweb homepage via icon

## v1.2.1 (2023-07-31)

### Fix

- bug where unverified users cannot log out

## v1.2.0 (2023-07-08)

### Feat

- add coord field to SiteData (table, modal)

### Fix

- allow filtering of sites on Sp. genus selection
- clone Draw layer prior to inplace transform (layer disappear)
- make model layers slightly transparent to see map
- map zindexes, disappearing layers on click
- broken cluster select after ol-ext update
- remote flatgeobuf bbox, load entire files
- remove coord transform on NewSite page

### Refactor

- allow Quercus Robur fgb model to be displayed
- update min width for map legend on small screen
- update translations
- revert map EPSG:4326 --> EPSG:3857 web mercator (distortion)
- move site modal coord to q-list
- add WGS 84 label to table heading

## v1.1.0 (2023-05-14)

### Feat

- mast model loading via flatgeobuf on data page
- add base layer selection
- add site details popup to data page
- replace ol Cluster with ol-ext AnimatedCluster
- add legend to map page, no cluster hulls
- allow users to delete profile from mastweb

### Fix

- fix elev chart labels if no data selected
- initial elevations on datapage, if no data
- error message on initial sites load fail
- auto-open map page legend if screen >md
- map page species selection getting wrong species
- legend formatting for desktop and mobile
- reactive species names and time chart range translations
- untoggle draw interaction after polygon draw
- persist locale from database in app

### Refactor

- update map EPSG:3857 --> EPSG:4326
- move site detail modal into component
- move profile page translations into separate file

## v1.0.0 (2023-04-02)

### Feat

- add map popup modal with site data

### Fix

- sendRaw for image uploader to send to s3
- translations for dataSource on data page

### Refactor

- rename model --> modal for datapage dialog

## v0.8.0 (2023-04-02)

### Feat

- add download instructions to profile page
- format profile page, add download user data button
- profile page formatting, use /me api, subscribe btn
- add additional aria accessibility labels to btns
- replace generic image upload with custom button
- update newSite models, translations, image upload field
- remove genus from new site page, filterable species

### Fix

- include data in data page that has no elevation set
- data page graphs squashed on mobile
- time graph update with filtered data selection
- restrict date obs in future months, fix popup close
- include legend in data page map
- login final fix using watchEffect
- remove otel boot file until traces k8s endpoint fixed

### Refactor

- move s3 models to data/models subdir

## v0.7.0 (2023-03-06)

### Feat

- display model from s3 button for 4 main species
- add dexie to recover from IndexDB on load (persist sites)

### Fix

- login logic, persistent user
- display previous years data if no values current year
- filter time series chart, include entire time range
- add constrainResolution to openlayers for crisper tiles
- migrate auth0-vue to v2 syntax
- add /logout page for case login stuck/looped
- update otel to only trace backend api calls (else auth0 errors)
- chart colors, year selection (-range), time chart range selector
- time chart relative rounding + xaxis sorting

### Refactor

- remove console.logs
- remove options api global props for axios
- change y axis label for relative charts
- reset basemap in user store, typing for null locale tooltip
- stats page sidebar translations
- map page year filter ascending order (latest first)
- datapage typings, add placeholder data download buttons
- remove site limit imposed for demo
- remove sites from user to general pinia store
- add type to imports for clarity TS v3.8, update chart types

### Perf

- minor update to map page filtering
- performance and readability of chart data filtering

## v0.6.0 (2023-01-21)

### Feat

- stats page date range, add time distribution graph
- add toggle for inversion of the elevation chart
- elevation distribution chart with locales and relative switcher

### Fix

- reactive xaxis for elevation chart (with site filter)
- add apexcharts as bootfile with all locales
- pwa app update duplicate refresh icon
- wrong icon displaying for profile/login (bool logic)

### Refactor

- remove dockerignore file, as container build not included
- remove versions.yaml from trapeze config
- remove otel endpoint check, allow graceful failure
- rename otel service name: browser --> mastweb
- update otel traces endpoint to be specific
- temp limit sites to 5000 for demo

## v0.5.0 (2022-12-06)

### Feat

- apexcharts boot file, loaded on app start
- data page extract elevs, breakpoints for phone layout, better filter
- table stats, elevation filter, table sorting
- add props to make openlayers component more generic, plus add legend
- filter data table by map selection, vertical scroll, fix typing
- working data table, with filtering and translations
- add text-color prop for select component
- add opentelemetry instrumentation for tracing

### Fix

- auth0 login in pinia store, persistent state, route guard
- service worker not registering in dev mode
- bug select genus when species common name is used
- add option for user to refresh on pwa app update
- only instrument telemetry if production
- no top-level await for otel axios checker
- otel tracing url, remove suffix (handled by proxy)
- otel endpoint variable prepend with APP\_
- add aria labelling for mobility, use webp encoded app icon

### Refactor

- add or statement to site load spinner to prevent NaN
- temporarily slice sites to first 10000 for demo
- replace pinia persistent storage plugin with vueuse browser storage
- remove src-capacitor and src-electron, using pwa instead
- move async initial data load to async onMounted
- remove redundant trackLocation prop for map
- wrap table stats for DataTable footer
- change zoom on click prop for cluster open layers
- pass props to openlayers, filter map points plus table
- otel url checker use post request, prevent excessive console warnings
- move site loading into store action, use quasar loading plugin (on app start)
- otel trace endpoint in env for build
- changed api mapdata endpoint to /data

## v0.4.0 (2022-11-06)

### Refactor

- NewSitePage attributes camelCase to kebab-case
- refine SelectFilter, simplify & update v-slot to #
- rename geoJSONArray --> geoJsonArray for kebab-camel props
- add new shortcut to pwa manifest to trigger app reloads
- extract geometry on user draw event, improve types
- add vite types to global tsconfig
- refine service worker error logging
- improve logging for service worker registration
- rename stats locale file to data (with page name change)
- rename StatsPage to DataPage to prevent ad-block trigger
- pull q-select dropdowns out into configurable component
- src --> @ for auth.ts
- prettier MapOpenLayers component
- change i18n file names to be expandable
- increment android app version to 22 (pwa uploaded manually)
- remove default icons for pwa

### Fix

- fix service worker by quasar building with dotenv package
- final fix to service worker with import.meta.env
- service worker registration failing due to process.env
- make axios functions async to prevent blocking i18n load
- move robots.txt and digital assets json to public dir
- content to stats locale files, preventing rollup build
- add user notifications if offline or app update (pwa)
- use capacitor for all mobile geolocation (not available in navigator)

### Feat

- allow common name for new site, auto-add genus on species select
- add map drawing prop, modify filter options
- update filter to accept multiple values, simplify
- add button to download excel template, fix upload btn format
- add digital assets links json for google app verification
- add robots.txt to prevent bot crawling
- add xlsx import from profile page, to /import endpoint
- start stats page, minimap, data table, layout
- update view for sidebars, add general store, optional sidebar toggle
- add custom service worker for pwa for POST background sync (offline)

## v0.3.0 (2022-10-05)

### Fix

- remove auth handling per device (use PWA), remove Azure AD WSL login
- logic bug preventing mobile web browser using geolocation
- bug where selected species does not change with locale update
- electron set custom url scheme default
- ios & android add support for custom url scheme

### Refactor

- update untranslated species from null to xxx

### Feat

- add pwa as new version of app (platform independent)

## v0.2.12 (2022-09-26)

### Fix

- placeholders for electron app browser close listener
- update auth0 init redirect_uri based on device platform

## v0.2.11 (2022-09-26)

### Fix

- ci pipeline fix apksigner adding dollar to pass env var

## v0.2.10 (2022-09-26)

### Fix

- capacitor handle redirect callback with event listener for browser close

## v0.2.9 (2022-09-03)

### Fix

- fix capacitor not including browser & geolocation plugins
- update electron protocol, allow for debug console, set icon to ico

### Refactor

- remove @capacitor/device, not required
- capacitor 4 browser version, run capacitor sync

## v0.2.8 (2022-09-01)

### Fix

- full translations throughout app - newsite, map, error pages (+notifications)
- translations for new site form
- add user notify to disable geolocation button (if unavailble)
- add spinner for new site upload progress

## v0.2.7 (2022-09-01)

### Fix

- capacitor browser open for auth in mobile app

## v0.2.6 (2022-09-01)

### Fix

- add extra details to user profile page, with translations

## v0.2.5 (2022-08-30)

### Fix

- ci pipeline cd into android dir, change auth json dir

## v0.2.4 (2022-08-30)

### Fix

- ci pipeline android publish aab location

## v0.2.3 (2022-08-30)

### Fix

- ci pipeline android artifact location

## v0.2.2 (2022-08-29)

### Fix

- typo in ci

## v0.2.1 (2022-08-29)

### Fix

- add windowName for capacitor browser auth webview

## v0.2.0 (2022-08-25)

### Feat

- finalise android gitlab pipeline, automated release

### Fix

- working android ci pipeline with build and release together

## v0.1.17 (2022-08-25)

### Fix

- ci pipeline cache policy to push for android build

## v0.1.16 (2022-08-24)

### Fix

- set correct artifactDir for gradle play publisher plugin

## v0.1.15 (2022-08-24)

### Fix

- remove userFraction from gradle play publisher config (redundant)

## v0.1.14 (2022-08-24)

### Fix

- change from is.mobile to is.nativeMobile (capacitor only)

## v0.1.13 (2022-08-24)

### Fix

- typing for pinia store + extractDate with toString

## v0.1.12 (2022-08-24)

### Fix

- remove comment to bump version, app icon fix --> app store

## v0.1.11 (2022-08-24)

### Fix

- use auzre ad login, fix mobile in app browser login (with capacitor), update logo

## v0.1.10 (2022-08-17)

### Fix

- gitlab pipeline variable substitution on release job, bump to re-tag

## v0.1.9 (2022-08-17)

### Fix

- remove invalid / breaking electron config

## v0.1.8 (2022-08-17)

### Fix

- remove console log on profile page, add todo

## v0.1.7 (2022-08-17)

### Refactor

- update scripts to correctly bump all versions

### Fix

- standard versioning via commitizen for all refs
- update app icon, increment version for cz testing

### [0.1.6](https://gitlab.com/mastweb/frontend/compare/v0.1.5...v0.1.6) (2022-08-16)

### Bug Fixes

- downgrade electron-builder dependency for @quasar/app-vite ([d9ea8d6](https://gitlab.com/mastweb/frontend/commitsd9ea8d698b20e4de255c9157d6bce4e4571a9b43))

### [0.1.5](https://gitlab.com/mastweb/frontend/compare/v0.1.4...v0.1.5) (2022-08-16)

### Bug Fixes

- map filters allow to deselect ([62fd51e](https://gitlab.com/mastweb/frontend/commits62fd51e8330bacea45d1753030682c1adf757f11))

### [0.1.4](https://gitlab.com/mastweb/frontend/compare/v0.1.3...v0.1.4) (2022-08-16)

### Bug Fixes

- update profile page, fix avatar sizing ([dc8b76a](https://gitlab.com/mastweb/frontend/commitsdc8b76ac37276794ca278223c9c950bd106c74b3))

### [0.1.3](https://gitlab.com/mastweb/frontend/compare/v0.1.2...v0.1.3) (2022-08-16)

### Bug Fixes

- fix vue router-link styling throughout app ([4359d19](https://gitlab.com/mastweb/frontend/commits4359d19fe2fcbd3b82c0f6cf8526aac220ac4a15))
- move user logout to profile page, flesh out ([cffc72e](https://gitlab.com/mastweb/frontend/commitscffc72e9604465996670575d9143c99f5a4facce))

### [0.1.2](https://gitlab.com/mastweb/frontend/compare/v0.1.1...v0.1.2) (2022-08-15)

### Bug Fixes

- update all species translations, working map filters ([6b67005](https://gitlab.com/mastweb/frontend/commits6b6700582d9cce9d5eb6bf1c22633ade425faf66))
- working map filters, add site refresh button ([c36cdd7](https://gitlab.com/mastweb/frontend/commitsc36cdd7d8bfebba7d225b2c40792aa7fae031a99))

### [0.1.1](https://gitlab.com/mastweb/frontend/compare/v0.0.0...v0.1.1) (2022-08-13)

### Bug Fixes

- remove mast type from map filters, include filter values from mappings ([94b4efb](https://gitlab.com/mastweb/frontend/commits94b4efbf71cc3ff2afd0c58b4cf555dcb3b4632c))
- update auth handling, token expiry with renewal after 1 day ([c99f3a3](https://gitlab.com/mastweb/frontend/commitsc99f3a37e809bccccfd3bb94275d3072f59cea04))
- update capacitor dependency versions due to errors ([28cbe05](https://gitlab.com/mastweb/frontend/commits28cbe056d466f4aa52bf4566ebd98feba07c1a3e))

## 0.1.0 (2022-08-10)

### Features

- add accessToken to user store, update definitions, cache sites ([932389b](https://gitlab.com/mastweb/frontend/commits932389bc4909f397eaa10cffdc3f207828c625ad))
- add humps package axios interceptor, snake-case to camelCase and viceversa ([c8d4537](https://gitlab.com/mastweb/frontend/commitsc8d45378ccdacff862946bb6ab5424f6a8b71592))
- add loading spinner for initial sites load ([f458889](https://gitlab.com/mastweb/frontend/commitsf458889c0719adb3097951faef7dd0f02b26efdc))
- add minimap option to ol map with custom css, update quasar css ([870182a](https://gitlab.com/mastweb/frontend/commits870182aa4664b750e6f3b6aead4d8441840b4d49))
- add skeleton requirements for auth0 integration ([2e35f3b](https://gitlab.com/mastweb/frontend/commits2e35f3b871e960b2eaf4a1b903fc6accc450c062))
- add userstore, working header with user and translations ([9308b74](https://gitlab.com/mastweb/frontend/commits9308b747381f4d8518803b96cfcf08adc3963f1b))
- link i18n locale to quasar component lang for internationalisation ([918027e](https://gitlab.com/mastweb/frontend/commits918027e66af8905cfdc2d13f341765376e60fe2f))
- new site form near completion, geolocation, update Site model ([d44d8ab](https://gitlab.com/mastweb/frontend/commitsd44d8ab6275501156143df157213991445756678))
- new site form with validation, full geolocation support, minimap ([04f1568](https://gitlab.com/mastweb/frontend/commits04f1568aee94526010287d2840064adb673a77eb))
- split prod/dev axios, load sites from api into MapPage with quasar notify on fail ([10cfc88](https://gitlab.com/mastweb/frontend/commits10cfc88f0baa6174e775bcc01b601317269be0f1))
- start LayerSwitcher, add map attributions, disable geo when denied ([a1d810f](https://gitlab.com/mastweb/frontend/commitsa1d810f2afbd914a9293512f3e9ce9d2fb10ec33))
- update header with icons and translations, add openlayers ([3652f20](https://gitlab.com/mastweb/frontend/commits3652f20bc709f2a169e3d56b4598f60c78cd45db))
- update map page with floating buttons, start new site page ([6b5998d](https://gitlab.com/mastweb/frontend/commits6b5998dead2d85b180a645f3ec66b2ed4f79af85))
- update newSite fields, working submit to API ([ea7a1fd](https://gitlab.com/mastweb/frontend/commitsea7a1fd223f4bdf1e1efd6449b7c4d9c43c00292))
- update newsitepage formatting, add fields available ([f040ef9](https://gitlab.com/mastweb/frontend/commitsf040ef97e3dd5509ec7fc374735b3fa516088dfa))
- work in progress, adding auth0 user to state ([c1e550d](https://gitlab.com/mastweb/frontend/commitsc1e550df74868d6fde166c458e16204c35e6eb42))
- working site cluster display on map page, no popups (in progress) ([5c9acc3](https://gitlab.com/mastweb/frontend/commits5c9acc3eb41fc86c39978da5cebab7e04ee5bf6b))

### Bug Fixes

- add alias throughout, glob wsl logo, fix lang pack dyanmic import ([812246a](https://gitlab.com/mastweb/frontend/commits812246a2dd716c1d561f73feeb5ddece2a6e6652))
- checking geoJSONArray is null before loading ([14329de](https://gitlab.com/mastweb/frontend/commits14329ded3f95b973a5daad345b9d43a89f1cb979))
- cluster layer typing, typo in layer ref ([454c73c](https://gitlab.com/mastweb/frontend/commits454c73c8f0a9cb47298c152faaf29651fbfd294d))
- restyle the NewSite page, add demo post request ([108629d](https://gitlab.com/mastweb/frontend/commits108629d0fee084b5b2ab92ec49a216b25508930e))
- support for i18n, fix slicing, add test to home page ([fbca142](https://gitlab.com/mastweb/frontend/commitsfbca1428067f71c3645eaa2f7395568637762d32))
- temp downgrade vite to v2.9.14 to allow build (v3.x.x conflicts) ([f8ca33f](https://gitlab.com/mastweb/frontend/commitsf8ca33fb49189ad00637be2ec9de77d1a49d9058))
- update datepickers to use v-close-popup, fix case when newDate is null ([bec1369](https://gitlab.com/mastweb/frontend/commitsbec1369b0806d4fd2aca2f726de3995232730005))
- update user store, set default locale from quasar, add quasar device pkg ([cfa10ae](https://gitlab.com/mastweb/frontend/commitscfa10ae467c658504a863f8426557c1d4937984f))
