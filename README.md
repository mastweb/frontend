# MastWeb Frontend

Frontend web and mobile application for the MastWeb project.

## Development

### Install the dependencies

```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).

### Generate Icons

```bash
npm install -D
node_modules/.bin/icongenie generate -m pwa -i /path/to/source/icon.png --theme-color 0d9aa2 --svg-color 0d9aa2 --png-color 0d9aa2
```

## Production

- To build for web / single page application:

```bash
quasar build
```

- Run the Gitlab pipeline for a tag and extract the build artifacts.
- Builds available on https://gitlabext.wsl.ch

### S3 Bucket Data

- In the `mastweb` S3 bucket, there is a folder named `data`.

```
S3
├── data
│   ├── models
│   │   └── 2015_abiesAlba.fgb
│   └── user_images
│       └── 099be610-3f2d-4b77-b8b7-3180543d4f04.jpg
└── instructions.pdf
```

- **models** are used to the load the models for each species on the data page.
  - The file names must be in format `<year>_<speciesNameScientific>.fgb`.
  - _The files must be Flatgeobuf format, in EPSG 4326 (WGS84)._
- **user_images** contains the uploaded images by users, named with the Site ID.
- `instructions.pdf` contains the instructions downloaded on the user profile page.
